<?php

// --- CSV gen

function genCSV(string $fileName, int $rows, string $separator)
{
    $filePath = path("csv-output:$fileName.csv");

    if (file_exists($filePath)) {
        unlink($filePath);
    }

    $fileStream = fopen($filePath, 'w+');
    $csvData = genCSVData($rows);

    foreach ($csvData as $fields) {
        fputs($fileStream, implode($separator, $fields) . "\n");
    }

    fclose($fileStream);
}

/**
 * @param int $rows
 * @return array
 */
function genCSVData(int $rows): array
{
    $data = [];

    for ($i = 0; $i < $rows; $i++) {
        $data[] = genCSVRow();
    }

    return $data;
}

/**
 * @return array
 */
function genCSVRow(): array
{
    $login = randomString(12);
    $password = randomString(12, true);
    $accountData = genAccountData(rand(2, 30));

    return [
        $login,
        $password,
        $accountData
    ];
}

/**
 * @param int $keyValues
 * @return string
 */
function genAccountData(int $keyValues = 10): string
{
    $data = [];

    for ($i = 0; $i < $keyValues; $i++) {
        $data[randomString(rand(5, 15))] = randomString(rand(3, 50), true);
    }

    return json_encode($data);
}

// --- Helpers

/**
 * $path format: directory:directory:file.php
 *
 * It's equals to (depending on system):
 * directory/directory/file.php
 * or
 * directory\directory\file.php
 *
 * @param string $pathString
 * @return string
 */
function path(string $pathString): string
{
    $path = BASE_DIR;
    $pathElements = explode(':', $pathString);

    foreach ($pathElements as $pathElement) {
        $path .= DIRECTORY_SEPARATOR . $pathElement;
    }

    return $path;
}

/**
 * @param int $length
 * @param bool $numbers
 * @return string
 */
function randomString(int $length = 10, bool $numbers = false): string
{
    $string = '';
    $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

    if ($numbers) {
        $chars .= '0123456789';
    }

    $charsLength = strlen($chars);

    for ($i = 0; $i < $length; $i++) {
        $string .= $chars[rand(0, $charsLength - 1)];
    }

    return $string;
}

/**
 * @param ...$vars
 */
function dd(...$vars)
{
    echo '<pre>';

    foreach ($vars as $var) {
        var_dump($var);
    }

    echo '</pre>';
    die;
}