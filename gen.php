<?php

if (empty($_POST['name']) && empty($_POST['rows']) && empty($_POST['separator'])) {
    return;
}

$name = (string) $_POST['name'];
$rows = (int) $_POST['rows'];
$separator = (string) $_POST['separator'];

genCSV($name, $rows, $separator);