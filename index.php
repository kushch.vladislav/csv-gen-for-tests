<?php

const BASE_DIR = __DIR__;

require_once 'functions.php';
require_once 'gen.php';

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <form action="index.php" method="post">
        <label for="rows">
            Row count (int)
        </label>
        <input type="text" id="rows" name="rows" value="10" required> <br>

        <label for="name">
            File name (without .csv)
        </label>
        <input type="text" id="name" name="name" value="import" required> <br>

        <label for="separator">
            Separator (1 char)
        </label>
        <input type="text" id="separator" name="separator" value=":" maxlength="1" required> <br>

        <input type="submit" value="Generate">
    </form>
</body>
</html>
